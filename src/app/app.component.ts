import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
selector: 'app-root',
templateUrl: './app.component.html',
styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
registerForm: FormGroup;
submitted = false;
piortityes: any = [
{
id: 1,
name: 'normal',
},
{
id: 2,
name: 'low',
},
{
id: 3,
name: 'high',
},
];
now: Date = new Date();
constructor(private formBuilder: FormBuilder) {}
public presets = [
{ label: 'Today', start: new Date(), end: new Date() },
{ label: 'This Month', start: new Date(new Date().setDate(1)), end: new Date() }
];
ngOnInit() {
this.registerForm = this.formBuilder.group({
title: ['', Validators.required],
description: [''],
piortity: [''],
dueDate: [''],
});
this.registerForm.get('dueDate').setValue(this.now.toLocaleDateString());
this.registerForm.get('piortity').setValue('normal');
}

// convenience getter for easy access to form fields
get f() {
return this.registerForm.controls;
}
selectPiortity(item:any){
this.registerForm.get('dueDate').setValue(item);
}
onSubmit() {
this.submitted = true;

// stop here if form is invalid
if (this.registerForm.valid) {
localStorage.setItem('data', JSON.stringify(this.registerForm.value));
console.log(this.registerForm.value);

}

}

}